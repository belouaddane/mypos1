<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">G-TRANS</a>
    </div>
    <div style="color: white;
                        padding: 15px 50px 5px 50px;
                        float: right;
                        font-size: 16px;"> <span></span>&nbsp; <!--<a href="#" class="btn btn-danger square-btn-adjust">Logout</a>-->
    </div>
    <div class="dropdown pull-right ">
        <button class="btn btn-danger btn-lg dropdown-toggle" type="button" data-toggle="dropdown"><span class="glyphicon glyphicon-globe"></span>
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <li>
                    <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                        {{ $properties['native'] }}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>

</nav>
<!-- /. NAV TOP  -->
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li class="text-center">
                <img src="{{asset('dashboard/assets/img/find_user.png')}}" class="user-image img-responsive"/>
                <span class="text-danger">{{auth()->user()->first_name.' '}}{{auth()->user()->last_name}}</span>
            </li>
            <li>
                <a  href="{{route('dashboard.index')}}"><i class="fa fa-dashboard fa-3x"></i> <span>@lang('site.dashboard')</span></a>
                @if(auth()->user()->hasPermission('read_users'))

                    <a  href="{{route('dashboard.users.index')}}"><i class="fa fa-users fa-3x"></i> <span>@lang('site.users')</span></a>

                    @endif
            </li>

        </ul>

    </div>

</nav>
