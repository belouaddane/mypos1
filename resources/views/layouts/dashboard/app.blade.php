<?php
/**
 * Created by PhpStorm.
 * User: elhou
 * Date: 01/09/2019
 * Time: 23:57
 */


?>


    <!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>G-STOCKS</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="{{asset('dashboard/assets/css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="{{asset('dashboard/assets/css/font-awesome.css')}}" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->

    <!-- CUSTOM STYLES-->
    <link href="{{asset('dashboard/assets/css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="{{asset('dashboard/assets/js/dataTables/dataTables.bootstrap.css')}}" rel="stylesheet" />
</head>
<body>
<div id="wrapper" class="">
@include('layouts.dashboard._aside')

@include('partials._session')
<!-- /. NAV SIDE  -->
    <div id="page-wrapper" >
        <div id="page-inner">
            @yield('content')
            <!-- /. ROW  -->
            <hr />



        </div>

    </div>
    <!-- /. PAGE INNER  -->
</div>
<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="{{asset('dashboard/assets/js/jquery-1.10.2.js')}}"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{asset('dashboard/assets/js/bootstrap.min.js')}}"></script>
<!-- METISMENU SCRIPTS -->
<script src="{{asset('dashboard/assets/js/jquery.metisMenu.js')}}"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="{{asset('dashboard/assets/js/dataTables/jquery.dataTables.js')}}"></script>
<script src="{{asset('dashboard/assets/js/dataTables/dataTables.bootstrap.js')}}"></script>
<!--<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
        /*$('.delete').click(function (e) {
            var that = $(this)

            e.preventDefault();
            var n = new Notification({
                text:"@lang('site.confirm_delete')",
                type:"warning",
                killer:true,
                button:[
                    Notification.button("@lang('site.yes')",'btn btn-success mr-2',function () {
                       that.closest('form').submit();
                    }),
                    Notification.button("@lang('site.no')",'btn btn-danger mr-2',function () {
                        n.close();
                    })
                ]
            })
        });*/
    });
</script>-->

<!-- CUSTOM SCRIPTS -->
<script src="{{asset('dashboard/assets/js/custom.js')}}"></script>


<script>


        $(".image").change(function () {


            if (this.files && this.files[0]){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(".image_preview").attr('src',e.target.result);

                }
                reader.readAsDataURL(this.files[0]);
            }
        });


</script>

</body>
</html>
