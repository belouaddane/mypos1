@extends('layouts.dashboard.app')



@section('content')
    <div class="row">
        <div class="container-fluid">
            <section class="card-header">
                <h1 align="right"><!--@lang('site.dashboard')--></h1>
                <ol class="breadcrumb">

                    <li><a href="{{route('dashboard.index')}}" style="text-decoration: none;color: dimgrey" class="" >@lang('site.dashboard') </a> </li>
                    <li><a href="{{route('dashboard.users.index')}}" style="text-decoration: none;color: dimgrey" class="" >@lang('site.users') </a> </li>
                    <li><a href="#" style="text-decoration: none;color: dodgerblue" class="active" ><i class="fa fa-edit"></i>@lang('site.edit') </a> </li>



                </ol>
            </section>
            <section class="content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('site.edit')
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('partials._errors')
                                <form  action="{{route('dashboard.users.update',$user->id)}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    {{method_field('put')}}
                                    <div class="form-group">
                                        <label>@lang('site.first_name')</label>
                                        <input type="text" name="first_name"  class="form-control" value="{{$user->first_name}}"  />
                                    </div>
                                    <div class="form-group">
                                        <label>@lang('site.last_name')</label>
                                        <input type="text" name="last_name"  class="form-control" value="{{$user->last_name}}"  />
                                    </div>
                                    <div class="form-group">
                                        <label>@lang('site.email')</label>
                                        <input type="email" name="email"  class="form-control" value="{{$user->email}}"  />
                                    </div>
                                    <div class="form-group">
                                        <label>@lang('site.image')</label>
                                        <input type="file" name="image" class="image"  />
                                    </div>
                                    <div class="form-group">
                                        <img src="{{$user->image_path}}" style="width: 50px" class="img-thumbnail image_preview">
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <label>@lang('site.permission')</label>
                                                </div>
                                                <div class="panel-body">
                                                    @php
                                                        $modals =['users', 'categories', 'products'];
                                                        $maps =['create', 'read', 'update', 'delete'];
                                                    @endphp
                                                    <ul class="nav nav-tabs">
                                                        @foreach($modals as $index=>$modal)
                                                            <li class="{{$index == 0 ? 'active' : ''}}"><a href="#{{$modal}}" data-toggle="tab">@lang('site.'.$modal)</a></li>
                                                        @endforeach

                                                    </ul>


                                                    <div class="tab-content">
                                                        @foreach($modals as $index=>$modal)
                                                            <div class="tab-pane fade {{$index == 0 ? 'active' : ''}} in" id="{{$modal}}">
                                                                @foreach($maps as $map)
                                                                    <label class=""><input type="checkbox" name="permissions[]" {{$user->hasPermission($map.'_'.$modal) ? 'checked' : ''}} value="{{$map.'_'.$modal}}">@lang('site.'.$map)</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                @endforeach
                                                            </div>
                                                        @endforeach
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-warning"><i class="fa fa-edit"></i>@lang('site.edit')</button>

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>

@endsection
