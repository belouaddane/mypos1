@extends('layouts.dashboard.app')



@section('content')
    <div class="row">
        <div class="container-fluid">
            <section class="card-header">
                <h1 align="right"><!--@lang('site.dashboard')--></h1>
                <ol class="breadcrumb">

                    <li><a href="{{route('dashboard.index')}}" style="text-decoration: none;color: dimgrey" class="" >@lang('site.dashboard') </a> </li>
                    <li><a href="{{route('dashboard.users.index')}}" style="text-decoration: none;color: dimgrey" class="" >@lang('site.users') </a> </li>
                    <li><a href="{{route('dashboard.users.create')}}" style="text-decoration: none;color: dodgerblue" class="active" ><i class="fa fa-plus"></i>@lang('site.add') </a> </li>



                </ol>
            </section>
            <section class="content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('site.add')
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                @include('partials._errors')
                                <form  action="{{route('dashboard.users.store')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    {{method_field('post')}}
                                    <div class="form-group">
                                        <label>@lang('site.first_name')</label>
                                        <input type="text" name="first_name"  class="form-control" value="{{old('first_name')}}"  />
                                    </div>
                                    <div class="form-group">
                                        <label>@lang('site.last_name')</label>
                                        <input type="text" name="last_name"  class="form-control" value="{{old('last_name')}}"  />
                                    </div>
                                    <div class="form-group">
                                        <label>@lang('site.email')</label>
                                        <input type="email" name="email"  class="form-control " value="{{old('email')}}"  />
                                    </div>
                                    <div class="form-group">
                                        <label>@lang('site.image')</label>
                                        <input type="file" name="image" class="image"  />
                                    </div>
                                    <div class="form-group">
                                        <img src="{{asset('uploads/user_images/default.png')}}" style="width: 50px" class="img-thumbnail image_preview">
                                    </div>
                                    <div class="form-group">
                                        <label>@lang('site.password')</label>
                                        <input type="password" name="password"  class="form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <label>@lang('site.password_confermation')</label>
                                        <input type="password" name="password_confermation"  class="form-control"  />
                                    </div>
                                    <div class="form-group">
                                        <div class="">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <label>@lang('site.permission')</label>
                                                </div>
                                                <div class="panel-body">
                                                    @php
                                                    $modals =['users', 'categories', 'products'];
                                                    $maps =['create', 'read', 'update', 'delete'];
                                                    @endphp
                                                    <ul class="nav nav-tabs">
                                                        @foreach($modals as $index=>$modal)
                                                        <li class="{{$index == 0 ? 'active' : ''}}"><a href="#{{$modal}}" data-toggle="tab">@lang('site.'.$modal)</a></li>
                                                        @endforeach

                                                    </ul>


                                                        <div class="tab-content">
                                                            @foreach($modals as $index1=>$modal1)
                                                            <div class="tab-pane fade {{$index1 == 0 ? 'active' : ''}} in" id="{{$modal1}}">
                                                                @foreach($maps as $map)
                                                                <label class=""><input type="checkbox" name="permissions[]" value="{{$map.'_'.$modal1}}">@lang('site.'.$map)</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    @endforeach
                                                            </div>
                                                            @endforeach
                                                        </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i>@lang('site.add')</button>

                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>



    @endsection
