@extends('layouts.dashboard.app')



@section('content')
    <div class="row">
        <div class="container-fluid">
            <section class="card-header">
                <h1 align="right">@lang('site.users')</h1>
                <ol class="breadcrumb">

                    <li><a href="{{route('dashboard.index')}}" style="text-decoration: none;color: dimgrey"  class="" >@lang('site.dashboard') </a> </li>
                    <li><a href="{{route('dashboard.users.index')}}" style="text-decoration: none;color: dodgerblue"  class="" ><i class="fa fa-users"></i>@lang('site.users') </a> </li>

                </ol>
            </section>
            <section class="content">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('site.users')<small>{{$users->total()}}</small>
                        <form action="{{route('dashboard.users.index')}}" method="get">
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{request()->search}}">
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i>@lang('site.search')</button>
                                    @if(auth()->user()->hasPermission('create_users'))
                                        <a href="{{route('dashboard.users.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i>@lang('site.add')</a>
                                        @else
                                        <a href="#" class="btn btn-primary disabled"><i class="fa fa-plus"></i>@lang('site.add')</a>
                                        @endif
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 ">
                                @if($users->count()> 0)
                                    <table class="table table-striped table-bordered table-hover" >
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>@lang('site.first_name')</th>
                                            <th>@lang('site.last_name')</th>
                                            <th>@lang('site.email')</th>
                                            <th>@lang('site.image')</th>
                                            <th>@lang('site.Action')</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $index=> $user)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td>{{$user->first_name}}</td>
                                                <td>{{$user->last_name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td><img src="{{$user->image_path}}" style="width: 50px" class="img-thumbnail"></td>
                                                <td>
                                                    @if(auth()->user()->hasPermission('update_users'))
                                                        <a href="{{route('dashboard.users.edit',$user->id)}}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>@lang('site.edit') </a>
                                                        @else
                                                        <a href="#" class="btn btn-xs btn-warning disabled"><i class="fa fa-edit"></i> @lang('site.edit') </a>
                                                        @endif
                                                    @if(auth()->user()->hasPermission('delete_users'))
                                                        <form action="{{route('dashboard.users.destroy',$user->id)}}" method="POST" style="display: inline-block">
                                                            {{csrf_field()}}
                                                            {{method_field('delete')}}
                                                            <button class="btn btn-xs btn-danger delete" onclick="return confirm('etes vous sur de vouloir supprimer ')"><i class="fa fa-trash-o"></i>@lang('site.delete')</button>
                                                        </form>
                                                        @else
                                                        <button class="btn btn-xs btn-danger disabled"><i class="fa fa-trash-o"></i>@lang('site.delete')</button>
                                                        @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                    {{$users->appends(request()->query())->links()}}
                                    @else
                                <h2>@lang('site.no_data_found')</h2>
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>


        </div>
    </div>
@endsection
