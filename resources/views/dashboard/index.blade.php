@extends('layouts.dashboard.app')

@section('content')

    <div class="row">
        <div class="container-fluid shadow-lg">
            <section class="card-header">
                <h1 align="right">@lang('site.dashboard')</h1>
                <ol class="breadcrumb">

                    <li><a href="{{route('dashboard.index')}}" style="text-decoration: none;color: dodgerblue" class="" ><i class="fa fa-dashboard"></i>@lang('site.dashboard') </a> </li>



                </ol>
            </section>

        </div>
    </div>

    @endsection
