<?php
/**
 * Created by PhpStorm.
 * User: elhou
 * Date: 08/10/2019
 * Time: 21:02
 */
return[
    'add'  =>  'Add',
    'create'  =>  'Add',
    'edit'  =>  'Update',
    'update'  =>  'Update',
    'delete'  =>  'Delete',
    'image'  =>  'Image',
    'read'  =>  'Read',
    'permission'  =>  'Permission',
    'search'  =>  'Search',
    'added_successfuly'  =>  'Added successfuly',
    'deleted_successfuly'  =>  'Deleted successfuly',
    'updated_successfuly'  =>  'updated successfuly',
    'no_data_found'  =>'No data found',
  'dashboard' => 'Dashboard',
  'users'     => 'Users',
  'permissions'     => 'Permissions',
    'first_name'     => 'First name',
    'last_name'     => 'Last name',
    'email'     => 'Email',
    'Action'     => 'Action',
    'categories'     => 'Categories',
    'products'     => 'Products',
    'password'     => 'Password',
    'password_confermation'     => 'Password confermation',
];
