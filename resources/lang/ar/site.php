<?php
/**
 * Created by PhpStorm.
 * User: elhou
 * Date: 08/10/2019
 * Time: 21:02
 */
return[
    'add'  =>  'اضف',
    'create'  =>  'اضف',
    'edit'  =>  'تعديل',
    'update'  =>  'تعديل',
    'read'  =>  'عرض',
    'delete'  =>  'حدف',
    'search'  =>  'بحث',
    'image'  =>  'الصورة',
    'password'  =>  'كلمة المرور',
    'password_confermation'  =>  'تاكيد كلمة المرور',
    'no_data_found'  =>'للاسف لا يوجد اي سجلات',
  'dashboard' => 'الرئيسية',
  'users'     => 'المشرفين',
    'first_name'     => 'النسب',
    'last_name'     => 'الاسم',
    'permission'     => 'الصلاحيات',
    'added_successfuly'     => 'تم بنجاح',
    'deleted_successfuly'     => 'تم بنجاح',
    'updated_successfuly'     => 'تم بنجاح',
    'email'     => 'البريد الالكتروني',
    'Action'     => 'اكشن',
    'permissions'     => 'الصلاحيات',
    'categories'     => 'الفئة',
    'products'     => 'منتجات',
];
