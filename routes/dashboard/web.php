<?php
/**
 * Created by PhpStorm.
 * User: elhou
 * Date: 09/10/2019
 * Time: 09:16
 */


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]],function()
    {
        Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function () {

            Route::get('/index', 'DashboardController@index')->name('index');


            Route::resource('users','UserController')->except(['show']);
        });
    });


